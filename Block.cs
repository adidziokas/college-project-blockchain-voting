﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BlockChain
{
    class Block : IBlock
    {
        public Block(string vote)
        {
            Nonce = 0;
            PrevHash = new byte[] {0x00};
            TimeStamp = DateTime.Now;
            Vote = vote;
        }
        public Block()
        {
            Nonce = 0;
            PrevHash = new byte[] { 0x00 };

        }
        public string Vote { get; set; }
        public byte[] Hash { get; set; }
        public int Nonce { get; set; }
        public byte[] PrevHash { get; set; }
        public DateTime TimeStamp { get; set; }

        public override string ToString()
        {
            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary["This Hash"] = Convert.ToBase64String(Hash);
            dictionary["Previous Hash"] = Convert.ToBase64String(PrevHash);
            dictionary["Nonce"] = Nonce.ToString();
            dictionary["Voted For"] = Vote;

            return JsonSerializer.Serialize(dictionary);
        }
    }
}
