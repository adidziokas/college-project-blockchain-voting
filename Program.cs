﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace BlockChain
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random(DateTime.UtcNow.Millisecond);
            BlockChain chain;
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "blockchain.txt";
            if (File.Exists(filePath))
            {
                chain = ReadBlocksFromFile(filePath);
            } else
            {
                IBlock genesisBlock = new Block("GENESIS"); //Treating the genesis block as if it has no value associated with it.
                chain = new BlockChain(genesisBlock);
            }
            Console.WriteLine("Checking BlockChain Validity");
            bool isChainValid = true;
            for (int i = 1; i < chain.Count; i++)
            {
                var thisBlockValid = BlockChainExtension.IsValid(chain[i]);
                var previousBlockValid = BlockChainExtension.IsValidPreviousBlock(chain[i], chain[i - 1]);
                Console.WriteLine("This block valid: ");
                Console.WriteLine(thisBlockValid);
                Console.WriteLine("Previous block valid: ");
                Console.WriteLine(previousBlockValid);
                if (!thisBlockValid | !previousBlockValid)
                {
                    isChainValid = false;
                }
            }
            if (!isChainValid)
            {
                Console.WriteLine("Chain Isn\'t valid, stopping program");
                return;
            }
            string vote = Voting();
            chain.AddNew(new Block(vote));
            List<string> candidateList = new List<string>();
            using (System.IO.StreamWriter file = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "blockchain.txt"))
            {
                foreach (var block in chain)
                {
                    candidateList.Add(block.Vote);
                    file.WriteLine(block.ToString());
                }
            }

            var voteCount = candidateList.GroupBy(x => x).Select(g => new {Name = g.Key, Count = g.Count()}).OrderByDescending(g => g.Count);

            foreach (var candidate in voteCount)
            {
                Console.WriteLine("{0} surinko {1} balsu", candidate.Name, candidate.Count);
            }
            
            
            var server = new Server();
            var client = new Client();
            Server.path = "P:\\";
            Task.Run(() => server.StartServer());
            Client.sendFile(AppDomain.CurrentDomain.BaseDirectory + "blockchain.txt"); //Needs to hold the directory + filename

        }

        static string Voting()
        {
            string balsas = "";
            bool choiceExists = true;
            do
            {
                Console.WriteLine("Balsuosite uz: ");
                Console.WriteLine("1. Gitanas Nauseda");
                Console.WriteLine("2. Ingrida Simonyte");
                Console.WriteLine("3. Saulius Skvernelis");
                Console.WriteLine("4. Vytenis Povilas Andriukaitis");
                Console.WriteLine("5. Arvydas Juozaitis");
                int pasirinkimas = int.Parse(Console.ReadLine());
                choiceExists = true;
                switch (pasirinkimas)
                {
                    case 1:
                        balsas = "Gitanas Nauseda";
                        break;
                    case 2:
                        balsas = "Ingrida Simonyte";
                        break;
                    case 3:
                        balsas = "Saulius Skvernelis";
                        break;
                    case 4:
                        balsas = "Vytenis Povilas Andriukaitis";
                        break;
                    case 5:
                        balsas = "Arvydas Juozaitis";
                        break;
                    default:
                        Console.WriteLine("Neegzistuojantis pasirinkimas");
                        choiceExists = false;
                        break;
                }
            } while (!choiceExists);
            return balsas;
        }
        static IBlock CreateGenesisBlock()
        {
            //Random rnd = new Random(DateTime.UtcNow.Millisecond);
            IBlock genesisBlock = new Block("GENESIS"); //Treating the genesis block as if it has no value associated with it.
            byte[] difficulty = new byte[] { 0x00, 0x00 };
            return genesisBlock;
        }
        static byte[] HashConvertStringToByte(string hashString) 
        {
            String[] arr = hashString.Split('-');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++) array[i] = Convert.ToByte(arr[i], 16);
            return array;
        }
        
        static BlockChain ReadBlocksFromFile(string filePath)
        {
            BlockChain chain = new BlockChain();
            var jsonLines = File.ReadLines(filePath);
            foreach(var line in jsonLines)
            {
                Block block = new Block();
                var deserializedJsonBlock = JsonSerializer.Deserialize<Dictionary<string, string>>(line);
                block.Hash = Convert.FromBase64String(deserializedJsonBlock["This Hash"]);
                block.Vote = deserializedJsonBlock["Voted For"];
                block.PrevHash = Convert.FromBase64String(deserializedJsonBlock["Previous Hash"]);
                block.Nonce = Convert.ToInt32(deserializedJsonBlock["Nonce"]);
                chain.AddExisting(block);
            }
            return chain;              
        }
    }
    
}
