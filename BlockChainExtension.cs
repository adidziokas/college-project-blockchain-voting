﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace BlockChain
{
    public static class BlockChainExtension
    {
        public static byte[] GenerateHash(this IBlock block)
        {
            using(SHA512 sha = new SHA512Managed())
            using(MemoryStream st = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(st))
            {
                bw.Write(block.Vote);
                bw.Write(block.Nonce);
                bw.Write(block.PrevHash);
                var stArr = st.ToArray();
                return sha.ComputeHash(stArr);
            }
        }

        public static byte[] MineHash(this IBlock block)
        {
            var difficulty = new byte[] { 0x00, 0x00 };
            byte[] hash = new byte[0];
            while (!hash.Take(2).SequenceEqual(difficulty))
            {
                block.Nonce++;
                hash = block.GenerateHash();
            }
            return hash;
        }

        public static bool IsValid(this IBlock block)
        {
            var bk = block.GenerateHash();
            return block.Hash.SequenceEqual(bk);
        }

        public static bool IsValidPreviousBlock(this IBlock block, IBlock previousBlock)
        {
            if(previousBlock == null) throw new ArgumentNullException(nameof(previousBlock));
            var prev = previousBlock.GenerateHash();
            return previousBlock.IsValid() && block.PrevHash.SequenceEqual(prev);
        }

        public static bool IsValid(this IEnumerable<IBlock> items)
        {
            var enumerable = items.ToList();
            return enumerable.Zip(enumerable.Skip(1), Tuple.Create).All(block =>
                block.Item2.IsValid() && block.Item2.IsValidPreviousBlock(block.Item1));
        }
    }
}
