﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlockChain
{
    class BlockChain : IEnumerable<IBlock>
    {
        private List<IBlock> _items = new List<IBlock>();

        public BlockChain(IBlock genesisBlock)
        {
            genesisBlock.Hash = genesisBlock.MineHash();
            Items.Add(genesisBlock);
        }
        public BlockChain()
        {

        }
        public void AddNew(IBlock item)
        {
            if (Items.LastOrDefault() != null)
            {
                item.PrevHash = Items.LastOrDefault()?.Hash;
            }

            item.Hash = item.MineHash();
            Items.Add(item);
        }
        public void AddExisting (IBlock item)
        {
            Items.Add(item);
        }
        public int Count => Items.Count;

        public IBlock this[int index]
        {
            get => Items[index];
            set => Items[index] = value;
        }
        public List<IBlock> Items
        {
            get => _items;
            set => _items = value;
        }
    public IEnumerator<IBlock> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
