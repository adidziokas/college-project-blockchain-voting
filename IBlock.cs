﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlockChain
{
    public interface IBlock
    {
        string Vote { get; set; }
        byte[] Hash { get; set; }
        int Nonce { get; set; }
        byte[] PrevHash { get; set; }
    }
}
